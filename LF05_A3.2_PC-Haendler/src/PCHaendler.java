import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		String artikel = liesString("Was moechten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesmwst("Geben Sie die MWST an:");
		
		double netto = berechnetGesamtnettopreis(anzahl, preis);
		double brutto = berechneGesamtbruttopreis(netto, mwst);
		
		rechungausgeben(artikel, anzahl, preis, netto, brutto, mwst);
		
	}
	
	public static String liesString (String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("was moechten Sie bestellen?");
		String artikel = myScanner.next();
		
		return artikel;
	}
	
	public static int liesInt (String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();
		
		return anzahl;
	}
	
	public static double liesDouble (String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();
		
		return preis;
	}
	
	public static double liesmwst (String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie die MWST an:");
		double mwst = myScanner.nextDouble();
		
		return mwst;
	}
	
	public static double berechnetGesamtnettopreis (int anzahl, double preis) {
		
		return anzahl * preis;
		
	}
	
	public static double berechneGesamtbruttopreis (double gesamtnettopreis, double mwst) {
		
	return gesamtnettopreis * (1 + mwst / 100);
	
	}
	
	public static void rechungausgeben (String artikel, int anzahl, double preis ,double netto, double brutto,
			double mwst) {
		
		System.out.println("\n\nRechnung");
		System.out.printf("Netto:  %-20s %6d %10.2f %n", artikel, anzahl, netto);
		System.out.printf("Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, brutto, mwst, "%");
		
	}

}
