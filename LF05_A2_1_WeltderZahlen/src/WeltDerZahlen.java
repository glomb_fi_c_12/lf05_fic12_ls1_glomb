/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 29.09.2021
  * @author << Lukas Glomb >>
  */

import java.util.Scanner;

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnensystem                    
    byte anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 200000000000L;
    
    // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       short alterTage = 6839;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm =  150000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       int flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
       float flaecheKleinsteLand = (float) 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Anzahl des Alters in Tage: " + alterTage);
    
    System.out.println("Schwerste Tier der Welt: " + gewichtKilogramm);
    
    System.out.println("Größte Land der Erde: " + flaecheGroessteLand);
    
    System.out.println("Kleinste Land der Erde: " + flaecheKleinsteLand);


    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

