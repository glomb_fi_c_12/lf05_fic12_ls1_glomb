package fahrkartenautomat;

public class Ticket {
	private String name;
	private double price;
	
	public Ticket(String name, double price) {
		this.name = name;
		this.price =  price;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public double getPrice() {
		return this.price;
	}
}
