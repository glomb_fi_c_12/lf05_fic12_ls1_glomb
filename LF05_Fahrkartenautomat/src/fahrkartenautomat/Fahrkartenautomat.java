package fahrkartenautomat;

import java.util.Scanner;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
 
public class Fahrkartenautomat {
	
	HashMap<Integer, Ticket> fahrkartenbestellung = new HashMap<Integer, Ticket>();
	
	public static void wait(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
 
	 
	public static Map<Ticket, Integer> fahrkartenbestellungErfassen() {
		Scanner keyboard = new Scanner(System.in).useLocale(Locale.US);
		
		Map<Integer, Ticket> tickets = new HashMap<Integer, Ticket>();
		tickets.put(1, new Ticket("Einzelfahrschein Regeltarif AB", 2.90));
		tickets.put(2, new Ticket("Tageskarte Regeltarif AB", 8.60));
		tickets.put(3, new Ticket("Kleingruppen-Tageskarte Regeltarif AB", 23.50));
		
		Map<Ticket, Integer> bestelltetickets = new HashMap<Ticket, Integer>();

		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
		tickets.forEach((id, ticket) -> System.out.println("    (" + id + ") " + ticket.getName() + " [" + String.format(Locale.US, "%.2f", ticket.getPrice()) + " EUR]"));
		System.out.println("");
		
		String mehrTickets = "Nein";
			do {
				Ticket ticket;
					do {
						System.out.print("Ihre Wahl: ");
						int ticketID = keyboard.nextInt();
						ticket = tickets.get(ticketID);
						
						if (ticket == null) {
							System.out.println(">>falsche Eingabe<<");
						}
					} while(ticket == null);
					
					int ticketAmount;
					do {
						System.out.print("Anzahl der Tickets: ");
						ticketAmount = keyboard.nextInt();
						
						if (ticketAmount < 1 || ticketAmount > 10) {
							System.out.println("Bitte wählen sie eine Nummer zwischen 1 und 10");
						}
					} while(ticketAmount < 1 || ticketAmount > 10);
					bestelltetickets.put(ticket, ticketAmount);
		
					keyboard.nextLine();
					System.out.print("\n Möchten sie mehr Tickets haben (Ja/Nein)");
					mehrTickets = keyboard.nextLine();
					
			} while (mehrTickets.equals("Ja"));
			return bestelltetickets;

	

	}
	

	// Pay order and return change amount
	public static double fahrkartenBezahlen(Map<Ticket, Integer> tickets) {
		double amountPaid = 0.0;
		double amountToPay = 0;
		double insertedCoin;
		double returnAmount;
		
		for(Map.Entry<Ticket, Integer> entry : tickets.entrySet()) {
			Ticket ticket = entry.getKey();
			Integer amount = entry.getValue();
			
			amountToPay += ticket.getPrice() * amount;
			
			
		}
 
		Scanner keyboard = new Scanner(System.in).useLocale(Locale.US);
 
		while(amountPaid < amountToPay) {
			System.out.printf(Locale.US, "Noch zu zahlen: %.2f€\n", (amountToPay - amountPaid));
 
			System.out.print("Eingabe (min. 0.05€, max. 2€): ");
			insertedCoin = keyboard.nextDouble();
			amountPaid += insertedCoin;
		}
 
		returnAmount = amountPaid - amountToPay;
 
		return returnAmount;
	}
 
	// Issue ticket
	public static void fahrkartenAusgeben() {
		String successMessage = "Fahrschein wird ausgegeben";
		System.out.println("\n\n"+ successMessage);
		for (int i = 0; i < successMessage.length(); i++) {
			System.out.print("=");
			wait(100);
		}
	}
 
	// Return change
	public static void rueckgeldAusgeben(double changeAmount) {
		System.out.printf(Locale.US, "Der Rückgabebetrag in Höhe von %.2f Euro\n wird in folgenden Münzen ausgezahlt", changeAmount);
 
		int coin2, coin1, coin50, coin20, coin10, coin5;
		coin2 = coin1 = coin50 = coin20 = coin10 = coin5 = 0;
 
		while(changeAmount >= 2.0) {
			coin2++;
			changeAmount -= 2.0;
		}
		while(changeAmount >= 1.0) {
			coin1++;
			changeAmount -= 1.0;
		}
		while(changeAmount >= 0.5) {
			coin50++;
			changeAmount -= 0.5;
		}
		while(changeAmount >= 0.2) {
			coin20++;
			changeAmount -= 0.2;
		}
		while(changeAmount >= 0.1) {
			coin10++;
			changeAmount -= 0.1;
		}
		while(changeAmount >= 0.05) {
			coin5++;
			changeAmount -= 0.05;
		}
 
		if (coin2 > 0) {
			System.out.println(coin2 + "x 2€");
		}
		if (coin1 > 0) {
			System.out.println(coin1 + "x 1€");
		}
		if (coin50 > 0) {
			System.out.println(coin50 + "x 0.50€");
		}
		if (coin20 > 0) {
			System.out.println(coin20 + "x 0.20€");
		}
		if (coin10 > 0) {
			System.out.println(coin10 + "x 0.10€");
		}
		if (coin5 > 0) {
			System.out.println(coin5 + "x 0.5€");
		}
	}
	    
 
		public static void main(String[] args) {
			while (true) {
		Map<Ticket, Integer> amountToPay = fahrkartenbestellungErfassen();

 
		double changeAmount = fahrkartenBezahlen(amountToPay);
 
		fahrkartenAusgeben();
 
		if(changeAmount > 0.0) {
			rueckgeldAusgeben(changeAmount);
		}
 
		System.out.println("\n\nVergessen Sie nicht, den Fahrschein "
							+ "\nvor Fahrtantritt entwerten zu lassen!"
							+ "\nWir wünschen Ihnen eine gute Fahrt.");
			}
		
		
	}
 
}
	    
	    
	    