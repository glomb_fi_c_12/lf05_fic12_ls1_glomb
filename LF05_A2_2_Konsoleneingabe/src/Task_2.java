import java.util.Scanner; // Import der Klasse Scanner
public class Task_2
{

 public static void main(String[] args) // Hier startet das Programm
 {

 // Neues Scanner-Objekt myScanner wird erstellt
 Scanner myScanner = new Scanner(System.in);

 System.out.print("Ich grüße Sie.\n\n");
 
 System.out.print("Bitte geben Sie ihren Namen an: \n");

 String name = myScanner.nextLine();

 System.out.print("Bitte geben Sie ihr Alter an: ");

 int alter = myScanner.nextInt();

 System.out.print("\nHallo " + name + ". Du bist " + alter + " Jahre alt.");
 myScanner.close();

 }
}