
public class konsolenausgabe2 {

	public static void main(String[] args) {

		// Code by Lukas Glomb
		
		//Task 1 
//		String s = "*";
//		
//		System.out.printf( "%5s\n", s + s ); 
//		System.out.printf( "%-3s %4s\n", s, s) ;
//		System.out.printf( "%-3s %4s\n", s, s) ;
//		System.out.printf( "%5s\n", s + s ); 

		// Task 2
//		System.out.printf( "%-5s= %-19s=%4d\n", "0!", "", 1 );
//		System.out.printf( "%-5s= %-19s=%4d\n", "1!", "1", 1 );
//		System.out.printf( "%-5s= %-19s=%4d\n", "2!", "1 * 2", 1 * 2 );
//		System.out.printf( "%-5s= %-19s=%4d\n", "3!", "1 * 2 * 3", 1 * 2 * 3 );
//		System.out.printf( "%-5s= %-19s=%4d\n", "4!", "1 * 2 * 3 * 4", 1 * 2 * 3 * 4 );
//		System.out.printf( "%-5s= %-19s=%4d\n", "5!", "1 * 2 * 3 * 4 * 5", 1 * 2 * 3 * 4 * 5 );
	

		// Task 3 | Aufgabe 3 
		System.out.printf("%-12s|%12s\n", "Fahrenheit" , "Celsius");
		System.out.printf("%s\n", "-".repeat(25));
		System.out.printf("%+-12d|%12.2f\n", -20  , -28.8889);
		System.out.printf("%+-12d|%12.2f\n", -10  , -23.3333);
		System.out.printf("%+-12d|%12.2f\n", 0  , -17.7778);
		System.out.printf("%+-12d|%12.2f\n", 20  , -6.6667);
		System.out.printf("%+-12d|%12.2f\n", 30  , -1.1111);
		
	}



}
