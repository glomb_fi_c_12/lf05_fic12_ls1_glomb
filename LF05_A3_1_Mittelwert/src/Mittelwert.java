
public class Mittelwert {

	public static void main(String[] args) {
	      
		// (E) "Eingabe"
	      // Werte für x und y festlegen:
	      // ===========================
	      double wert1 = 2.0;
	      double wert2  = 4.0;
	      double durchschnitt = berechnetMittelwert(wert1, wert2);
	      
	      ausgabe(wert1, wert2, durchschnitt);
	}

//	public static void eingabe(String [] args) {
//		
//		
//		
//	}
	
	public static double berechnetMittelwert(double wert1, double wert2) {
		
		double durchschnitt = (wert1 + wert2)/2.0;
		
		return durchschnitt;
	}
	
	public static void ausgabe(double wert1, double wert2, double durchschnitt) {
		
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", wert1, wert2, durchschnitt);
	}
	
	
	}
